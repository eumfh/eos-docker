# EOS-Docker

EOS-Docker offers an easy way to run the EOS application via virtual machines on any machine that Docker supports. The project consists of several containers which, in addition to the EOS software, also offer the database with its own web interface.


## Setup

Clone the repository


```
git clone https://git.iccas.de/eumfh/eos-docker
cd eos-docker
```

To start the project, an account has to be registered in the [ICCAS Docker-Hub](https://docker.iccas.de). Your account will be granted access to the EOS project by the [ICCAS-Team](https://www.iccas.de).

All required Docker containers can be pulled and started via the `docker-compose.yml` file. The following 3 steps must be carried out for this:

1. Login:

```
docker login docker.iccas.de
```

2. Pull:

```
docker compose pull
```

3. Run:

```
docker compose up -d
```
## Test

Run http://localhost:8080 to see if the application is running.

# Use the Application

The default version of the database is publicly accessible via the network address of the host on `port 27017`. EOS can be reached via `port 8080` of the host. The web interface of the database can be reached via `port 8081`.
The ports can be adjusted in the `docker-compose.yml` file.


## Shut-down

To terminate the system, run

```
Strg^c
docker-compose down
```
